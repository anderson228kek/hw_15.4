#include <iostream>
#include <windows.h>
using namespace std;

void f(int x)
{
    int N, i;
    cout << "Число N: ";
    cin >> N;
    cout << endl;
    if (x == 1)
    {
        cout << "Нечётные - ";
    }
    else
    {
        cout << "Чётные - ";
    }
    for (i = x; i < N + 1; i += 2)
    {
        cout << i << " ";
    }
}

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int N, i, x;
    cout << "Число N: ";
    cin >> N;
    cout << endl;
    cout << "Чётные числа: ";
    for (i = 1; i < N + 1; i++)
    {
        if (i % 2 == 0)
        {
            cout << i << " ";
        }
    }
    cout << endl << endl;

    cout << "1 - нечётные, 2 - чётные\n";
    cin >> x;
    if (x == 1 or x == 2)
    {
        f(x);
    }
    else
    {
        cout << "неверное значение";
    }
}
